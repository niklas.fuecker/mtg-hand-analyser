from hand_analyser.deck import Deck, deck_load, Player, Goldfish_Game


def test_shuffle():
    deck = Deck(deck_load("./Scales.txt"))
    prev = deck.Maindeck.copy()
    deck.shuffle()
    assert prev != deck.Maindeck


def test_draw_card():
    deck = Deck(deck_load("./Scales.txt"))
    prev = deck.Maindeck.copy()

    card = deck.drawCard()
    assert card
    assert len(deck.Maindeck) + 1 == len(prev)

    deck.Maindeck.insert(0, card)

    assert prev == deck.Maindeck


def test_get_card():
    assert False


def test_put_card():
    deck = Deck(deck_load("./Scales.txt"))
    prev = deck.Maindeck.copy()

    card = deck.drawCard()
    assert card
    assert len(deck.Maindeck) + 1 == len(prev)

    deck.putCard(card, 0)

    assert prev == deck.Maindeck


def test_deck():
    deck = Deck(deck_load("./Scales.txt"))

    assert deck.Maindeck


def test_draw_hand():
    deck = Deck(deck_load("./Scales.txt"))
    player = Player(deck)
    player.drawHand()
    assert len(player.hand) == 7


def test_init_game():
    assert False


def test_hand_str():
    deck = Deck(deck_load("./Scales.txt"))
    player = Player(deck)
    player.drawHand()
    print(player.hand_str())
    assert len(player.hand) == 7


def test_eval_hand():
    deck = Deck(deck_load("./Scales.txt"))
    player = Player(deck)
    game = Goldfish_Game(player)
    game.win_conditions.add("p.is_in('hand','name:Arcbound Ravager') and p.is_in('hand','name:Arcbound Worker')")
    # game.win_conditions.add("p.is_in('hand','Arcbound Ravager') or p.is_in('hand','Arcbound Worker')")
    res = game.eval_Hand(turn=3, simulations=1000)
    print(res[0])
    assert res


def test_add_win_con():
    deck = Deck(deck_load("./Scales.txt"))
    player = Player(deck)
    game = Goldfish_Game(player)
    game.add_win_con("@turn3(is_in(hand, ravager,2 ))", name="test")
    print(game.win_conditions)
    game.add_win_con("@turn2(is_in(hand, ravager,2 ))", name="test")
    assert game.win_conditions
    print(game.win_conditions)


def test_eval_wincons():
    deck = Deck(deck_load("./Scales.txt"))
    player = Player(deck)
    game = Goldfish_Game(player)
    # game.add_win_con("@turn3(self.is_in('hand','name : Arcbound Ravager','0' ))", name="test")
    # print(game.win_conditions)
    game.add_win_con(
        "@turn40(self.is_in('hand','name : Arcbound Ravager','4' ) and self.is_in('hand','name: Arcbound Worker',1))",
        name="test")
    # game.add_win_con("@turn40(self.is_in(('hand','name : Arcbound Ravager','4' ))", name="test")
    print(game.evalWincons(draw=True))


def test_is_in():
    deck = Deck(deck_load("./Scales.txt"))
    player = Player(deck)
    game = Goldfish_Game(player)
    player.is_in('hand', 'name : Arcbound Ravager', '2')
    assert False


def test_run_sim():
    deck = Deck(deck_load("./Scales.txt"))
    player = Player(deck)
    game = Goldfish_Game(player)
    game.add_win_con(
        """@turn1(self.is_in('hand','name : Hardened Scales , The Ozolith , Arcbound Worker','3' ) 
          )""",
        name="test")
    print(game.run_Sim())

