import re
import json

def json_to_cardlist(in_dict):
    """
    uses dict decklist, to create a list of individual cards that can be shuffled
    :param in_json:
    :return:
    """
    cardlist= []
    exclude_keys = {'qty'}
    for card in in_dict:
        for i in range(0,int(card["qty"])):
            keyset = set(card.keys()).difference(exclude_keys)
            cardlist.append({key: card[key] for key in keyset})
    return cardlist

def deck_to_dict(in_str, dict_keys={"name","qty","tags", "types"}, json_file="./AllCards.json"):
    def get_card_json(cardname, count, tags):
        nonlocal json_data
        nonlocal dict_keys

        # get card data
        try:
            card = json_data[cardname]
            card["qty"] = count
            card["tags"] = tags
            card["name"] = cardname
            temp_keys = dict_keys
            if not dict_keys:
                temp_keys = card.keys()
            else:
                temp_keys.add("qty")
            return {key: card[key] for key in temp_keys}

        except:
            print(str(cardname) + " not found")
        pass
    def prepare_card_format(in_str):
        nonlocal add_to_sb
        # filter if SB
        if re.match('(^\s*sb:*)',in_str,flags=re.I):
            in_str = re.split('(^\s*sb:*)',in_str,flags=re.I)[-1]
            add_to_sb = True

        # get count

        res  = re.match(r'^\s*(\d+)\s*(.*)',in_str,flags=re.I)
        count = res.group(1)
        in_str = res.group(2)

        # get Tags
        name, *tags= in_str.split('#')
        
        return str.strip(name), count, tags



    Mainboard = []
    Sideboard = []
    with open(json_file) as f:
        json_data = json.load(f)
        f.close()
    sb_mode = False

    for entry in str.splitlines(in_str):
        add_to_sb = False
        if re.search(r'.*(sideboard).*', entry, flags=re.I):
            sb_mode = True
            continue
        if sb_mode:
            add_to_sb = True
        name, count, tags = prepare_card_format(entry)



        if add_to_sb:
            Sideboard.append(get_card_json(name,count, tags))
        else:
            Mainboard.append(get_card_json(name, count, tags))
    return {"Mainboard": Mainboard, "Sideboard":Sideboard}







def deck_load(link):
    with open(link,"r", encoding='utf-8') as file:
        text_list = file.read()
        file.close()
        return text_list




