import copy
import random
from typing import List

from hand_analyser.deck_convert import *

class Deck(object):
    Author: str
    Maindeck: list
    Sideboard: list

    deckfile: object
    def __init__(self, deckfile, keys = None):
        deckdict = deck_to_dict(deckfile, dict_keys=keys)

        self.Maindeck = json_to_cardlist(deckdict["Mainboard"])
        self.Sideboard = json_to_cardlist(deckdict["Sideboard"])



    def shuffle(self):
        random.shuffle(self.Maindeck)
    def drawCard(self):
        try:
            res = self.Maindeck[0]
            del self.Maindeck[0]
            return res
        except:
            return None
    def getCard(self,name):
        pass
    def putCard(self,card,pos,board= None):
        if not board:
            board = self.Maindeck
        board.insert(pos,card)





class Player(object):
    deck : Deck
    library: list
    hand: list
    grave: list
    battlefield:list
    exile:list
    sideboard = dict

    def __init__(self, deck:Deck):
        self.deck = deck
        self.library = deck.Maindeck
        self.sideboard = deck.Sideboard
        self.hand=[]
        self.grave=[]
        self.battlefield=[]
        self.exile=[]
    def drawHand(self, cards=7):
        for i in range(0,cards):
            self.hand.append(self.deck.drawCard())
    def drawCard(self):
        self.hand.append(self.deck.drawCard())
    def init_game(self):
        self.deck.shuffle()
        self.drawHand()
    def hand_str(self):
        name_list=[]
        for elem in self.hand:
            name_list.append(elem["name"])
        return ','.join(name_list)
    def is_in(self,zone,arg_str,number=1,duplicates=False, verbose= False):
        """
        # TODO fix
        :param zone: The zone hwich to check
        :param arg_str: arg=value to check, by default name
        :param number: how many matches
        :param duplicates: if individual cards are allowed to count twice
        :return: if condition satisfied
        """
        def matches(elem):
            nonlocal args
            for a in args:
                if str.lower(elem[a["arg"]].strip()) == str.lower(a["value"].strip()):
                    return True
        realzone =  eval("self."+ zone)

        if type(arg_str) == str:
            arg_str = [arg_str]
        args =  []
        # generate values
        for elem in arg_str:
            arg , values = elem.split(":")

            arg = arg.strip()
            value = values.split(",")
            for e in value:
                e = e.strip()
                args.append({"arg": arg, "value": e})

        res = [x for x in realzone if matches(x)]
        if len(res) >= int(number) and verbose:
            print([x["name"] for x in res])
        return len(res) >= int(number)


    def evalWinCon(self, conditions ):
        """
        Analyses wheter the condition ablies to the current payler object
        :param conditions:
        :return: bool
        """
        instructions = conditions
        for instruction in conditions:
            res = eval(instruction.replace("\n"," "))
            if res:
                return True, self
                #print([x["name"] for x in self.hand])

        return False, self

    def reset_deck(self):
        self.library += self.hand + self.exile + self.grave + self.battlefield
        self.hand = []
        self.exile = []
        self.grave = []
        self.battlefield= []
        self.deck.shuffle()

class Game(object):
    players : List[Player]
    starting_player: Player

    turn_log : list

    def __init__(self,players):
        self.players = players
        for p in self.players:
            p.init_game()
        self.win_conditions= dict()


    def add_win_con(self, in_str, name='', desc=''):

        turn, condition = re.match(r'.*@(?:t|turn)(\d+)\(((?:.|\s)*)\)', in_str).groups()
        turn = int(turn)
        # check if the wincon exists
        if name and name in self.win_conditions.keys():

            if turn in self.win_conditions[name]["conditions"].keys():
                self.win_conditions[name]["conditions"][turn].append(condition)
            else:
                self.win_conditions[name]["conditions"][turn] = [condition]
        else:
        # get turn, noted with @turn_nr
            self.win_conditions[name]= {
                        "desc": desc,
                        "conditions":{
                            turn: [condition]
                        }}




class Goldfish_Game(Game):
    def __init__(self,player):
        self.players = [player]
        self.starting_player = player
        self.win_conditions = dict()

        for p in self.players:
            p.init_game()


    def evalWincons(self,draw=False):
        """
        Evaluates the wincons on a game/ wincons can be either /or
        :param draw: if player is on the draw
        :return:
        """
        # now begin evaluation
        satifies_wincons = [False, []]
        for wincon_name, wincon_info in self.win_conditions.items():
            turn_log=[]

            self.starting_player.reset_deck()
            self.starting_player.drawHand()
            turn_log.append([copy.copy(self.starting_player)])



            for turn in range(max(list(wincon_info["conditions"].keys()))+2):

                # load old successes
                curr_game_state = turn_log[turn]
                # draw a card
                for game_player in curr_game_state:
                    if (draw and turn == 1) or turn >= 1:
                        self.starting_player.drawCard()

                # check for success
                curr_turn = []
                if turn in wincon_info["conditions"].keys():
                    # here check for wincons and get succesfull config back
                    for game_player in curr_game_state:

                        res, values  = game_player.evalWinCon(wincon_info["conditions"][turn])
                        if res:
                            curr_turn.append(values)
                else:
                    curr_turn = curr_game_state



                # append
                turn_log.append(curr_turn)

            if turn_log[max(list(wincon_info["conditions"].keys()))+1]:
                # then a wincon was found
                satifies_wincons=[True,satifies_wincons[1]]
                satifies_wincons[1].append(wincon_name)
        return satifies_wincons

    def run_Sim(self,hands=1000, draw = False):
        count = 0
        dict = {}
        for i in range(0,hands):
            tmp = self.evalWincons(draw=draw)
            if tmp[0]:
                count +=1
                for elem in tmp[1]:
                    if not elem in dict.keys():
                        dict[elem] = 0
                    dict[elem] += 1
        return count/hands ,dict




